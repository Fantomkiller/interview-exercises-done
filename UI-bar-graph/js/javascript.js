let jsonData = [];

document.addEventListener("DOMContentLoaded", function () {

    document.getElementById("file-selector").addEventListener("change", function () {
        function handleEvent(event) {
            if (event.type === "load") {
                let data = JSON.parse(fileread.result);
                jsonData = Object.values(data).flat();
            }
        }

        function addListeners(fileread) {
            fileread.addEventListener('load', handleEvent);
        }

        let file_to_read = document.getElementById("file-selector").files[0];
        let fileread = new FileReader();

        if (file_to_read) {
            addListeners(fileread);
            fileread.readAsText(file_to_read);
        }
    })
});

function clearChart() {
    let chartToClear = document.getElementById("chart");
    while (chartToClear.firstChild) {
        chartToClear.removeChild(chartToClear.firstChild);
    }
}

function createChart() {

    clearChart();
    jsonData.forEach(value => {
        let chartValue = document.createElement("li");
        chartValue.innerHTML = `<span class='text'>chart Value ${value}%</span>`;
        chartValue.className = "chart-bar fill-value-" + value;

        document.getElementById("chart").appendChild(chartValue);
    });


}


