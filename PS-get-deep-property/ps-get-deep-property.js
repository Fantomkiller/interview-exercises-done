const person = {person: {name: {first: 'FirstName', middleInitial: 'I', lastName: 'LastName'}}};

const getDeepProperty = (objectNested, pathString) => {
    let pathArray = pathString.split('.');
    return pathArray.reduce((obj, key) =>
        (obj && obj[key] !== 'undefined') ? obj[key] : undefined, objectNested);
}

console.log(getDeepProperty(person, 'person.name.lastName'));
