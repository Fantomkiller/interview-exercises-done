// made reverse method cause of performance differences in other methods

class PalindromeUtil {

    static reverse(s) {
        let o = [];
        for (let i = 0, len = s.length; i <= len; i++)
            o.push(s.charAt(len - i));
        return o.join('');
    }

    static isPalindrome(word, words) {
        return word === this.reverse(word) && !words.includes(word)
    }

}

class SecondLongestPalindrome {
    constructor(text) {
        this.text = text
    }

    get SLP() {
        return this.count();
    }

    count() {
        let subStrings = [];
        let result = '';
        for (let i = 0; i < this.text.length; i++) {
            for (let j = 0; j < this.text.length - i; j++) {
                let subString = this.text.substring(j, j + i + 1);
                if (PalindromeUtil.isPalindrome(subString, subStrings)) {
                    subStrings.push(subString);
                }
            }
        }

        let filteredSubStrings = subStrings.filter(str => {
            return str.length >= 2
        });

        if (filteredSubStrings.length < 1) {
            result = 'No Palindrome exists';
            return result
        }
        if (filteredSubStrings[filteredSubStrings.length - 2]) {
            result = "Found Palindrome: " + filteredSubStrings[filteredSubStrings.length - 2];
            return result
        }
        if (!filteredSubStrings[filteredSubStrings.length - 2]) {
            result = 'No Second Palindrome exists';
            return result
        }


        return null;
    }
}

const referrer = new SecondLongestPalindrome('referrer');

console.log(referrer.SLP);

